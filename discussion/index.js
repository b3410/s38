// console.log('Ohayo');

// DOM Selectors

//First you have to describe the location where in it's going to target/access elements.

//Identify the attribute and value to properly recognize and identify which element to target
//visulalize the statement in javascript
//let/const jsObject = {}

// const firstName = document.querySelector('#firstName');
// const lastName = document.querySelector('#lastName');
//check if you were able to successfully target an element from the document

//[SECTION] querySelector
//check the type of data that we targeted from the document

//[SECTION] getElementById => target a single component
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');
console.log(firstName);
console.log(lastName);

// console.log(typeof firstName);
//object

//get element by class name => can be essential when targetting mutltiple elements at the same time
const inputFields = document.getElementsByClassName('form-control');
console.log(inputFields);

////getElementsByTagName => can be used when targetting elements of the same tags
const heading = document.getElementsByTagName('h3');
console.log(heading);

//get the info from the input fields
//target the value property of the object
console.log(firstName.value);
